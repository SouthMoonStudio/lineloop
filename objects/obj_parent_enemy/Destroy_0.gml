///destroy
switch (object_index)
{
	case obj_enemy_0:
	{
		repeat(4)
		{
			var _system = global.part_system;
			var _length = random(24);
			var _dir = random(359);
			var _x = x+lengthdir_x(_length,_dir);
			var _y = y+lengthdir_y(_length,_dir);
			var _part = global.part0;
			var _count = 1;
		
			part_particles_create(_system,_x,_y,_part,_count);
		}
		var _color = merge_color($5600FF,$000000,0.25);
		repeat(2)
		{
			var _inst = instance_create_depth(x,y,depth,obj_enemy_gib);
			_inst.v_scale = 0.5+random(0.5);
			_inst.v_color = _color;
		}
		if instance_exists(obj_controller_camera)
		{
			obj_controller_camera.v_shake_intensity += 2;
		}
		break;
	}
	case obj_enemy_1:
	{
		repeat(3)
		{
			var _system = global.part_system;
			var _length = random(32);
			var _dir = random(359);
			var _x = x+lengthdir_x(_length,_dir);
			var _y = y+lengthdir_y(_length,_dir);
			var _part = global.part1;
			var _count = 1;
		
			part_particles_create(_system,_x,_y,_part,_count);
		}
		var _color = merge_color($0077FF,$000000,0.25);
		repeat(7)
		{
			var _inst = instance_create_depth(x,y,depth,obj_enemy_gib);
			_inst.v_scale = 1+random(2);
			_inst.v_color = _color;
			_inst.v_speed = 3;
		}
		if instance_exists(obj_controller_camera)
		{
			obj_controller_camera.v_shake_intensity += 4;
		}
		break;
	}
}

var _pitch = 0.9+random(0.2);
audio_sound_pitch(snd_explode,_pitch);
audio_play_sound(snd_explode,10,false);
