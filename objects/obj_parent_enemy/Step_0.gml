///step
var _inst = instance_place(x,y,obj_parent_enemy);

if _inst != noone
{
	var _pd = point_direction(x,y,_inst.x,_inst.y)
	x -= lengthdir_x(0.2,_pd);
	y -= lengthdir_y(0.2,_pd);
}