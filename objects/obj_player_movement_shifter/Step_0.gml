///step
var _inst = instance_place(x,y,obj_player);

if _inst != noone
{
	var _pd = point_distance(x,y,_inst.x,_inst.y)
	switch (_inst.v_move_direction)
	{
		case e_move_direction.horizontal:
		{
			_inst.v_move_direction = e_move_direction.vertical;
			while _pd < 240 || y < 64 || y > room_height-64
			{
				x = _inst.x;
				y = random(room_height);
				_pd = point_distance(x,y,_inst.x,_inst.y);
			}
			global.pscore ++;
			obj_controller_background.v_scale = 1.5;
			break;
		}
		case e_move_direction.vertical:
		{
			_inst.v_move_direction = e_move_direction.horizontal;
			while _pd < 240 || x < 64 || x > room_width-64
			{
				x = random(room_width);
				y = _inst.y;
				_pd = point_distance(x,y,_inst.x,_inst.y);
			}
			global.pscore ++;
			obj_controller_background.v_scale = 1.5;
			break;
		}
	}
	var _pitch = 0.9+random(0.2);
	audio_sound_pitch(snd_collect,_pitch);
	audio_play_sound(snd_collect,10,false);
}

