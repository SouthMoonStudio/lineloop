///draw
//draw player
var _sprite_index = spr_player_movement_shifter;
var _image_index = 0;
var _x = x+4;
var _y = y+4;
var _scale = 1+0.25*dsin(current_time/3.6);
var _angle = 0;
var _color = $000000;
var _alpha = 0.2;

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_scale,_scale,_angle,_color,_alpha);

//draw player
var _sprite_index = spr_player_movement_shifter;
var _image_index = 0;
var _x = x;
var _y = y;
var _scale = 1+0.25*dsin(current_time/3.6);
var _angle = 0;
var _color = $00ffff;
var _alpha = 1;

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_scale,_scale,_angle,_color,_alpha);

if global.pscore = 0
{
	draw_sprite_ext(spr_instructions,2,x,y,1,1,0,$ffffff,1);
}