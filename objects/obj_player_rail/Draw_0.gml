//draw rail
var _sprite_index = spr_player_rail;
var _image_index = 0;
var _length = point_distance(x,y,v_beam_end_x,v_beam_end_y);
var _x = x;
var _y = y;
var _xscale = _length;
var _yscale = 1+0.5*dsin(v_railtime);
var _angle = v_direction;
var _color = $ffffff;
var _alpha = dsin(v_railtime);

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_xscale,_yscale,_angle,_color,_alpha)

//draw rail glob
var _sprite_index = spr_player_rail_glob;
var _image_index = 0;
var _x = x;
var _y = y;
var _xscale = 1+0.5*dsin(v_railtime);
var _yscale = 1+0.5*dsin(v_railtime);
var _angle = 0;
var _color = $ffffff;
var _alpha = dsin(v_railtime);

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_xscale,_yscale,_angle,_color,_alpha)
