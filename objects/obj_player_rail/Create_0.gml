///create
v_beam_end_x = 0;
v_beam_end_y = 0;

alarm[0] = 1;

v_railtime = 0;

if instance_exists(obj_controller_camera)
{
	obj_controller_camera.v_camera_flash = 1;
}

var _pitch = 0.9+random(0.2);
audio_sound_pitch(snd_special,_pitch);
audio_play_sound(snd_special,10,false);