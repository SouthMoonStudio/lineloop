///alarm 0 
var _xstart = x;
var _ystart = y;
while _xstart > 0 && _xstart < room_width && _ystart > 0 && _ystart < room_height
{
	_xstart += lengthdir_x(1,v_direction);
	_ystart += lengthdir_y(1,v_direction);
}
v_beam_end_x = _xstart;
v_beam_end_y = _ystart;

var _x1 = x;
var _x2 = v_beam_end_x;
var _y1 = y;
var _y2 = v_beam_end_y;
var _list = ds_list_create();
var _num = collision_line_list(_x1,_y1,_x2,_y2,obj_parent_enemy,false,true,_list,false);
if _num > 0
    {
    for (var i = 0; i < _num; ++i;)
        {
			with(_list[| i])
			{
				v_hitpoints -= 999;
			}
        }
    }
ds_list_destroy(_list);