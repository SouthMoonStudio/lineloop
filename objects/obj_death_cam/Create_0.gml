///create
v_px = room_width/3+random(room_width/3);
v_py = room_height/3+random(room_height/3);
gravity = 0.5;
gravity_direction = point_direction(x,y,v_px,v_py);
speed = 2;
direction = irandom(360);
v_time_reset = 180;
v_time = v_time_reset;
