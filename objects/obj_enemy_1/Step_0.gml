///step
event_inherited();

if instance_exists(obj_player)
{
	v_move_direction = point_direction(x,y,obj_player.x,obj_player.y);
}

x += lengthdir_x(v_move_speed,v_move_direction);
y += lengthdir_y(v_move_speed,v_move_direction);

if v_hitpoints <= 0
{
	instance_destroy();
}