///alarm 1 - enemy0
alarm[3] = 360;
if global.pscore < 40
{
	exit;
}

var _spawn = false;

while _spawn == false
{
	var _side = irandom(3)
	switch (_side)
	{
		case 0:
		{
			if camera_get_view_y(view_camera[0]) > 24
			{
				var _x = random(room_width);
				var _y = 0;
				var _depth = -10;
				var _object = obj_enemy_0;
				instance_create_depth(_x,_y,_depth,_object);
				_spawn = true;
			}
			break;
		}
		case 1:
		{
			if camera_get_view_y(view_camera[0]) < room_height-24-camera_get_view_height(view_camera[0])
			{
				var _x = random(room_width);
				var _y = room_height;
				var _depth = -10;
				var _object = obj_enemy_0;
				instance_create_depth(_x,_y,_depth,_object);
				_spawn = true;
			}
			break;
		}
		case 2:
		{
			if camera_get_view_x(view_camera[0]) > 24
			{
				var _x = 0;
				var _y = random(room_height);
				var _depth = -10;
				var _object = obj_enemy_0;
				instance_create_depth(_x,_y,_depth,_object);
				_spawn = true;
			}
			break;
		}
		case 3:
		{
			if camera_get_view_x(view_camera[0]) < room_width-24-camera_get_view_width(view_camera[0])
			{
				var _x = room_width;
				var _y = random(room_height);
				var _depth = -10;
				var _object = obj_enemy_0;
				instance_create_depth(_x,_y,_depth,_object);
				_spawn = true;
			}
			break;
		}
	}
}