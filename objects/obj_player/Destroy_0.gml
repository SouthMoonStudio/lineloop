///destroy
repeat(4)
{
	var _system = global.part_system;
	var _length = random(24);
	var _dir = random(359);
	var _x = x+lengthdir_x(_length,_dir);
	var _y = y+lengthdir_y(_length,_dir);
	var _part = global.part3;
	var _count = 1;
		
	part_particles_create(_system,_x,_y,_part,_count);
}
var _color = $000000;
repeat(12)
{
	var _inst = instance_create_depth(x,y,depth,obj_player_gib);
	_inst.v_scale = 1+random(1);
	_inst.v_color = _color;
	_inst.v_speed = choose(2,3,4);
}
if instance_exists(obj_controller_camera)
{
	obj_controller_camera.v_shake_intensity += 8;
}

instance_create_depth(x,y,0,obj_death_cam);

//update camera
with (obj_controller_camera)
{
	v_focus_type = e_focus_type.instance
	v_focus_index = obj_death_cam;
}

//update game state
obj_controller_game.v_game_state = e_game_state.gameover;