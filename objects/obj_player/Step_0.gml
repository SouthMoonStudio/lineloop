///step
if v_death == true
{
	v_color_mix = lerp(v_color_mix,1,0.05);
	
	if v_color_mix > 0.9
	{
		instance_destroy();
		if audio_is_playing(snd_change)
		{
			audio_stop_sound(snd_change);
		}
		var _pitch = 0.9+random(0.2);
		audio_sound_pitch(snd_explode,_pitch);
		audio_play_sound(snd_explode2,10,false);
	}
}

if instance_exists(obj_player_rail) || v_death == true
{
	exit;
}

var _input_left = keyboard_check(ord("A"));
var _input_right = keyboard_check(ord("D"));
var _input_up = keyboard_check(ord("W"));
var _input_down = keyboard_check(ord("S"));

var _move_speed_value = -_input_left+_input_right-_input_up+_input_down;
_move_speed_value = clamp(_move_speed_value,-1,1);

v_move_speed += _move_speed_value*v_move_speed_thrust;
v_move_speed = sign(v_move_speed)*max(abs(v_move_speed)-v_move_speed_friction,0);
v_move_speed = sign(v_move_speed)*min(abs(v_move_speed),v_move_speed_max);

switch (v_move_direction)
{
	case e_move_direction.horizontal:
	{
		x += v_move_speed;
		if x < 24
		{
			x = 24;
		}
		if x > room_width-24 
		{
			x = room_width-24;
		}
		break;
	}
	case e_move_direction.vertical:
	{
		y += v_move_speed;
		if y < 24 
		{
			y = 24;
		}
		if y > room_height-24 
		{
			y = room_height-24;
		}
		break;
	}
}

if v_reload > 0
{
	v_reload --;
}
else
{
	if v_overheat_time <= 0
	{
		if mouse_check_button(mb_left) && v_overheat_flag == false
		{
			var _x = x;
			var _y = y;
			var _depth = depth;
			var _object = obj_player_bullet;
			var _pd = point_direction(x,y,mouse_x,mouse_y);
			var _inst = instance_create_depth(_x,_y,_depth,_object);
			_inst.v_move_direction = _pd+random(v_scatter_value/v_heat_max*v_heat_current)*choose(1,-1);
			v_reload = v_reload_speed;
			v_heat_current = min(v_heat_current+v_heat_increase,v_heat_max);
			if v_heat_current == v_heat_max
			{
				v_overheat_time = v_overheat_time_max;
				v_overheat_flag = true;
			}
		}
		else
		{
			v_heat_current = max(v_heat_current-v_heat_cooling,0);
			if v_heat_current <= 0
			{
				v_overheat_flag = false;
			}
		}
	}
	else
	{
		v_overheat_time --;
		if mouse_check_button_pressed(mb_left)
		{
			var _pitch = 0.9+random(0.2);
			audio_sound_pitch(snd_overheat,_pitch);
			audio_play_sound(snd_overheat,10,false);
		}
	}
}

if v_rail_reload <= 0
{
	if mouse_check_button_pressed(mb_right)
	{
			var _pd = point_direction(x,y,mouse_x,mouse_y);
			var _x = x+lengthdir_x(24,_pd);
			var _y = y+lengthdir_y(24,_pd);
			var _depth = depth;
			var _object = obj_player_rail;
			var _inst = instance_create_depth(_x,_y,_depth,_object);
			_inst.v_direction = _pd;
			v_rail_reload = v_rail_reload_max;
	}
}
else
{
	v_rail_reload --;
}

if instance_place(x,y,obj_parent_enemy)
{
	v_death = true;
	var _pitch = 0.9+random(0.2);
	audio_sound_pitch(snd_change,_pitch);
	audio_play_sound(snd_change,10,false);
}