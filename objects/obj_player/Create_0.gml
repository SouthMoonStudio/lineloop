///create
sprite_index = spr_player_mask;
image_index = 0;
image_speed = 0;

v_move_direction = e_move_direction.horizontal;

v_move_speed = 0;
v_move_speed_max = 8;
v_move_speed_thrust = 1;
v_move_speed_friction = 0.5;

v_reload_speed = 5;
v_reload = v_reload_speed;

v_scatter_value = 15;

v_heat_max = 30;
v_heat_current = 0;
v_heat_cooling = 0.5;
v_heat_increase = 2;

v_overheat_time = 0;
v_overheat_time_max = 180;
v_overheat_flag = false;

v_rail_reload_max = 540;
v_rail_reload = 0;

//create movement shifter
var _x = choose(room_width/4,room_width/4*3);
var _y = y;
var _depth = depth;
var _object = obj_player_movement_shifter;
instance_create_depth(_x,_y,_depth,_object);

//update camera
with (obj_controller_camera)
{
	v_focus_type = e_focus_type.instance
	v_focus_index = other.id
}

v_death = false;
v_color_mix = 0;