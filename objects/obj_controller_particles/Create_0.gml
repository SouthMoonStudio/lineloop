///create
global.part_system = part_system_create();

//part0
var _part = part_type_create();
part_type_sprite(_part,spr_part0,true,true,false);
part_type_size(_part,1,1.25,0,0);
part_type_alpha2(_part,1,0);
part_type_speed(_part,0,0,0,0);
part_type_direction(_part,0,0,0,0);
part_type_orientation(_part,0,359,0,0,0);
part_type_gravity(_part,0,0);
part_type_color1(_part,$5600FF);
part_type_life(_part,10,30);
global.part0 = _part;

//part1
var _part = part_type_create();
part_type_sprite(_part,spr_part1,true,true,false);
part_type_size(_part,1,1.25,0,0);
part_type_alpha2(_part,1,0);
part_type_speed(_part,0,0,0,0);
part_type_direction(_part,0,0,0,0);
part_type_orientation(_part,0,359,0,0,0);
part_type_gravity(_part,0,0);
part_type_color1(_part,$0077FF);
part_type_life(_part,10,30);
global.part1 = _part;

//part2
var _part = part_type_create();
part_type_sprite(_part,spr_part2,true,true,false);
part_type_size(_part,1,1,-0.1,0);
part_type_alpha2(_part,1,0);
part_type_speed(_part,0,0,0,0);
part_type_direction(_part,0,0,0,0);
part_type_orientation(_part,0,0,0,0,0);
part_type_gravity(_part,0,0);
part_type_color1(_part,$ffffff);
part_type_life(_part,10,30);
global.part2 = _part;

//part3
var _part = part_type_create();
part_type_sprite(_part,spr_part0,true,true,false);
part_type_size(_part,1,1.25,0,0);
part_type_alpha2(_part,1,0);
part_type_speed(_part,0,0,0,0);
part_type_direction(_part,0,0,0,0);
part_type_orientation(_part,0,359,0,0,0);
part_type_gravity(_part,0,0);
part_type_color1(_part,$000000);
part_type_life(_part,10,30);
global.part3 = _part;
