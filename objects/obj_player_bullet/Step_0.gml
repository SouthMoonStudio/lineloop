///step
v_move_speed = sign(v_move_speed)*max(abs(v_move_speed)-v_move_speed_friction,0);
v_alpha -= v_alpha_step;

if v_alpha <= 0
{
	instance_destroy();
}

repeat(abs(v_move_speed))
{
	x += lengthdir_x(v_move_speed_size,v_move_direction);
	y += lengthdir_y(v_move_speed_size,v_move_direction);
	
	var _system = global.part_system;
	var _x = x;
	var _y = y;
	var _part = global.part2;
	var _count = 1;
		
	part_particles_create(_system,_x,_y,_part,_count);
}

var _inst = instance_place(x,y,obj_parent_enemy)

if _inst != noone
{
	with (_inst)
	{
		v_hitpoints --;
	}
	instance_destroy();
}

if x <= 0
{
	instance_destroy();
}
if x >= room_width
{
	instance_destroy();
}
if y <= 0
{
	instance_destroy();
}
if y >= room_height
{
	instance_destroy();
}