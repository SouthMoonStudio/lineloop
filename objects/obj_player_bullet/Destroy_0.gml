///destroy
var _color = merge_color($ffffff,$000000,0.25);
repeat(3)
{
	var _inst = instance_create_depth(x,y,depth,obj_player_gib);
	_inst.v_scale = 0.5+random(0.5);
	_inst.v_color = _color;
	_inst.v_speed = 0.25;
}

var _pitch = 0.9+random(0.2);
audio_sound_pitch(snd_hit,_pitch);
audio_play_sound(snd_hit,10,false);