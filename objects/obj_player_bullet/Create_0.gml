///create
sprite_index = spr_player_bullet_mask;
image_index = 0;
image_speed = 0;

v_move_speed = 160;
v_move_speed_friction = 3;
v_move_speed_size = 0.1;

v_alpha = 1;
v_alpha_step = 1/(v_move_speed*v_move_speed_size)/2;

var _pitch = 0.9+random(0.2);
audio_sound_pitch(snd_lazer,_pitch);
audio_play_sound(snd_lazer,5,false);
