///draw
//draw player shadow
var _sprite_index = spr_player_bullet;
var _image_index = 0;
var _x = x+4;
var _y = y+4;
var _scale = 1;
var _angle = 0;
var _color = $000000;
var _alpha = 0.2;

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_scale,_scale,_angle,_color,_alpha);

//draw player
var _sprite_index = spr_player_bullet;
var _image_index = 0;
var _x = x;
var _y = y;
var _scale = 1;
var _angle = 0;
var _color = $ffffff;
var _alpha = v_alpha;

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_scale,_scale,_angle,_color,_alpha);