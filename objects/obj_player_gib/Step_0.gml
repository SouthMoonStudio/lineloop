x += lengthdir_x(v_speed,v_dir);
y += lengthdir_y(v_speed,v_dir);

v_speed = max(v_speed-v_friction,0);

if v_speed <= 0
{
	if instance_exists(obj_controller_background)
	{
		var _surf = obj_controller_background.v_surf;
		if surface_exists(_surf)
		{
			surface_set_target(_surf);
				draw_sprite_ext(sprite_index,image_index,x,y,v_scale,v_scale,image_angle,v_color,1);
			surface_reset_target();
		}
	}
	instance_destroy();
}