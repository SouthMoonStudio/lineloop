///create
v_pause = false;

var _w = display_get_gui_width();
var _h = display_get_gui_height();
v_surf = surface_create(_w,_h);
surface_set_target(v_surf);
	draw_clear_alpha($000000,0);
surface_reset_target();

v_game_state = e_game_state.playing;

v_gameover_index = irandom(global.quote_count);

if !audio_is_playing(snd_bg_music)
{
	audio_play_sound(snd_bg_music,100,true);
}