///step
switch (v_game_state)
{
	case e_game_state.playing:
	{
		if keyboard_check_pressed(vk_escape)
		{
			v_pause = !v_pause;
			if v_pause = true
			{
				surface_set_target(v_surf);
					var _w = display_get_gui_width();
					var _h = display_get_gui_height();
					draw_surface_part(application_surface,0,0,_w,_h,0,0);
				surface_reset_target();
				instance_deactivate_all(true);
			}
			else
			{
				surface_set_target(v_surf);
					draw_clear_alpha($000000,0);
				surface_reset_target();
				instance_activate_all();
			}
		}
		if keyboard_check_pressed(ord("R"))
		{
			room_restart();
			global.pscore = 0;
		}
		if keyboard_check_pressed(ord("Q"))
		{
			game_end();
		}
		break
	}
	case e_game_state.gameover:
	{
		if keyboard_check_pressed(ord("R"))
		{
			room_restart();
			global.pscore = 0;
		}
		if keyboard_check_pressed(ord("Q"))
		{
			game_end();
		}
	}
}