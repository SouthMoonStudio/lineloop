///draw
switch (v_game_state)
{
	case e_game_state.playing:
	{
		if v_pause == true
		{
			var _w = display_get_gui_width();
			var _h = display_get_gui_height();
			if !surface_exists(v_surf)
			{
				v_surf = surface_create(_w,_h);
				surface_set_target(v_surf);
					draw_clear_alpha($000000,0);
				surface_reset_target();
			}
			draw_set_alpha(1);
			draw_surface(v_surf,0,0)
			draw_set_alpha(0.5);
			draw_set_color($000000);
			draw_rectangle(0,0,_w,_h,false);
			
			
			var _x = _w/2;
			var _y = _h/8;
			var _font = font_xlarge;
			var _halign = fa_center;
			var _valign = fa_center;
			var _color = $ffffff;
			var _alpha = 1;
			var _string = "Game Paused";
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text(_x,_y,_string);
		
			var _x = _w/2;
			var _y = _h/2;
			var _font = font_small;
			var _halign = fa_center;
			var _valign = fa_center;
			var _color = $ffffff;
			var _alpha = 0.5+0.4*dsin(current_time/3.6);
			var _string = "Press 'Escape' to resume";
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text(_x,_y,_string);
			
			var _x = 16;
			var _y = _h-16;
			var _font = font_xsmall;
			var _halign = fa_left;
			var _valign = fa_bottom;
			var _color = $ffffff;
			var _alpha = 1
			var _string = "Press 'R' to restart or 'Q' to quit"
			var _scale = 1;
			var _angle = 0;
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text_transformed(_x,_y,_string,_scale,_scale,_angle);
		}
		break;
	}
	case e_game_state.gameover:
	{
			var _w = display_get_gui_width();
			var _h = display_get_gui_height();
			
			draw_set_alpha(0.5);
			draw_set_color($000000);
			draw_rectangle(0,0,_w,_h,false);
			
			var _x = _w/2;
			var _y = _h/8;
			var _font = font_xlarge;
			var _halign = fa_center;
			var _valign = fa_center;
			var _color = $ffffff;
			var _alpha = 1;
			var _string = "Game Over";
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text(_x,_y,_string);
		

			var _x = _w/2;
			var _y = _h/3;
			var _font = font_small;
			var _halign = fa_center;
			var _valign = fa_center;
			var _color = $ffffff;
			var _alpha = 1
			var _string = global.gameover_quote[v_gameover_index];
			var _scale = 1;
			var _angle = 15*dsin(current_time/7.2);
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text_transformed(_x,_y,_string,_scale,_scale,_angle);
			
			var _x = _w/2;
			var _y = _h/3*2;
			var _font = font_medium;
			var _halign = fa_center;
			var _valign = fa_center;
			var _color = $ffffff;
			var _alpha = 1
			var _string = "Your final score:\n"+string(global.pscore);
			var _scale = 1;
			var _angle = 0;
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text_transformed(_x,_y,_string,_scale,_scale,_angle);
			
			var _x = 16;
			var _y = _h-16;
			var _font = font_xsmall;
			var _halign = fa_left;
			var _valign = fa_bottom;
			var _color = $ffffff;
			var _alpha = 1
			var _string = "Press 'R' to restart or 'Q' to quit"
			var _scale = 1;
			var _angle = 0;
		
			draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
			draw_text_transformed(_x,_y,_string,_scale,_scale,_angle);
		break;
	}
}