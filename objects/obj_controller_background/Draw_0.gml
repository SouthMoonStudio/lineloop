///draw
var _view_camera = view_camera[0];
var _x1 = camera_get_view_x(_view_camera);
var _y1 = camera_get_view_y(_view_camera);
var _x2 = camera_get_view_x(_view_camera)+camera_get_view_width(_view_camera);
var _y2 = camera_get_view_y(_view_camera)+camera_get_view_height(_view_camera);
var _outline = false;
var _alpha = 1;
var _color = merge_color(global.color_background[v_color[0]],global.color_background[v_color[1]],v_color_current);

draw_set_alpha(_alpha);
draw_set_color(_color);

draw_rectangle(_x1,_y1,_x2,_y2,_outline)

if !surface_exists(v_surf)
{
	v_surf = surface_create(room_width,room_height);
}

draw_surface(v_surf,0,0);


//draw score
var _x = room_width/2;
var _y = room_height/2;
var _font = global.font_score;
var _halign = fa_center;
var _valign = fa_center;
var _color = $000000;
var _alpha = 0.3;
var _string =string(global.pscore);
var _scale = 2+v_scale;

draw_set_text_parameters(_font,_halign,_valign,_color,_alpha);
draw_text_transformed(_x,_y,_string,_scale,_scale,0);

if global.pscore = 0
{
	draw_sprite_ext(spr_instructions,0,room_width/2-240,room_height/2+240,1,1,0,$ffffff,1);
	draw_sprite_ext(spr_instructions,1,room_width/2+240,room_height/2+240,1,1,0,$ffffff,1);
}
