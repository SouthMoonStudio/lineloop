///create
depth = 10;
v_color_current = 0;
v_color_target = 0;
v_color_timer_reset = 300;
v_color_timer = v_color_timer_reset;

v_color[0] = irandom(4);
v_color[1] = irandom(4);

v_scale = 1;

v_surf = surface_create(room_width,room_height);
surface_set_target(v_surf);
	draw_clear_alpha($000000,0);
surface_reset_target();
