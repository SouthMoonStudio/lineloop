///step
if v_color_timer > 0
{
	v_color_timer --;
}
else
{
	v_color_timer = v_color_timer_reset;
	switch (v_color_target)
	{
		case 0:
		{
			v_color_target = 1;
			v_color[1] += 1+irandom(3);
			if v_color[1] > 4
			{
				v_color[1] -= 5;
			}
			break;
		}
		case 1:
		{
			v_color_target = 0;
			v_color[0] += 1+irandom(3);
			if v_color[0] > 4
			{
				v_color[0] -= 5;
			}
			break;
		}
	}
}

v_color_current = lerp(v_color_current,v_color_target,0.1);

v_scale = lerp(v_scale,1,0.1);
