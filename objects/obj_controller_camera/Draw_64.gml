///draw gui
//draw crosshair
var _sprite_index = spr_crosshair;
var _image_index = 0;
var _x = device_mouse_x_to_gui(0);
var _y = device_mouse_y_to_gui(0);
var _scale = 1;
var _angle = 0;
var _color = $ffffff;
var _alpha = 1;

draw_sprite_ext(_sprite_index,_image_index,_x,_y,_scale,_scale,_angle,_color,_alpha);

if instance_exists(obj_player)
{
	//draw heat cooldown
	var _detail = 36;
	var _radius = 32;
	var _thickness = 4;
	var _x = device_mouse_x_to_gui(0);
	var _y = device_mouse_y_to_gui(0);
	var _max = 1;
	var _cur = 1;
	var _overheat = false;
	if instance_exists(obj_player)
	{
		var _max = obj_player.v_heat_max;
		var _cur = obj_player.v_heat_current;
		var _overheat = obj_player.v_overheat_flag;
	}
	var _color = $ffffff;
	if _overheat == true {_color = $0000ff}
	var _alpha = 1;

	draw_set_color(_color);
	draw_set_alpha(_alpha);

	draw_primitive_begin(pr_trianglestrip);
		for (var _a=0;_a<_detail;_a++)
		{
			if _a < _detail/_max*_cur
			{
				var _xx = _x-lengthdir_x(_radius-_thickness,10+160/_detail*_a);
				var _yy = _y+lengthdir_y(_radius-_thickness,10+160/_detail*_a);
				draw_vertex(_xx,_yy);
				var _xx = _x-lengthdir_x(_radius+_thickness,10+160/_detail*_a);
				var _yy = _y+lengthdir_y(_radius+_thickness,10+160/_detail*_a);
				draw_vertex(_xx,_yy);
			}
		}
	draw_primitive_end();

	//draw rail cooldown
	var _detail = 36;
	var _radius = 32;
	var _thickness = 4;
	var _x = device_mouse_x_to_gui(0);
	var _y = device_mouse_y_to_gui(0);
	var _max = 1;
	var _cur = 1;
	if instance_exists(obj_player)
	{
		var _max = obj_player.v_rail_reload_max;
		var _cur = obj_player.v_rail_reload;
	}
	var _color = $0000ff;
	var _alpha = 1;

	draw_set_color(_color);
	draw_set_alpha(_alpha);

	draw_primitive_begin(pr_trianglestrip);
		for (var _a=0;_a<_detail;_a++)
		{
			if _a < _detail/_max*_cur
			{
				var _xx = _x-lengthdir_x(_radius-_thickness,10+160/_detail*_a);
				var _yy = _y-lengthdir_y(_radius-_thickness,10+160/_detail*_a);
				draw_vertex(_xx,_yy);
				var _xx = _x-lengthdir_x(_radius+_thickness,10+160/_detail*_a);
				var _yy = _y-lengthdir_y(_radius+_thickness,10+160/_detail*_a);
				draw_vertex(_xx,_yy);
			}
		}
	draw_primitive_end();
}