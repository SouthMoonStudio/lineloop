
///step
switch (v_focus_type)
{
	case e_focus_type.instance:
	{
		if v_focus_index != noone
		{
			var _camera_index = view_camera[0];
			var _camera_width = camera_get_view_width(_camera_index);
			var _camera_height = camera_get_view_height(_camera_index);
			var _xx = camera_get_view_x(_camera_index);
			var _yy = camera_get_view_y(_camera_index);
			var _focus_index = v_focus_index;
			if instance_exists(_focus_index)
			{
				var _xxto = (_focus_index.x+mouse_x)/2-_camera_width/2;
				var _yyto = (_focus_index.y+mouse_y)/2-_camera_height/2;
				_xx = lerp(_xx,_xxto,0.07);
				_xx = round(_xx);
				_yy = lerp(_yy,_yyto,0.07);
				_yy = round(_yy);
			
			}
		
			camera_set_view_pos(_camera_index,_xx,_yy);
		}
		break;
	}
	case e_focus_type.point: 
	{
		var _camera_index = view_camera[0];
		var _camera_width = camera_get_view_width(_camera_index);
		var _camera_height = camera_get_view_height(_camera_index);
		var _xx = camera_get_view_x(_camera_index);
		var _yy = camera_get_view_y(_camera_index);
		var _xxto = v_focus_point_x-_camera_width/2;
		var _yyto = v_focus_point_y-_camera_height/2;
			
		_xx = lerp(_xx,_xxto,0.25);
		_xx = round(_xx);
		_yy = lerp(_yy,_yyto,0.25);
		_yy = round(_yy);
			
		camera_set_view_pos(_camera_index,_xx,_yy);
		break;
	}
}

if v_shake_intensity > 0
{
	v_shake_intensity = max(v_shake_intensity-v_shake_cooldown,0);
	v_shake_intensity = min(v_shake_intensity,v_shake_intensity_max);
	var _camera_index = view_camera[0];
	var _xx = camera_get_view_x(_camera_index)+v_shake_intensity*choose(1,-1);
	var _yy = camera_get_view_y(_camera_index)+v_shake_intensity*choose(1,-1);
			
	camera_set_view_pos(_camera_index,_xx,_yy);
}

v_camera_flash = max(v_camera_flash-v_camera_flash_cooldown,0);