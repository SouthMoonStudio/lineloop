///draw
//draw camera flash
if v_camera_flash > 0
{
	var _camera = view_camera[0];
	var _camera_x1 = camera_get_view_x(_camera);
	var _camera_x2 = camera_get_view_x(_camera)+camera_get_view_width(_camera);
	var _camera_y1 = camera_get_view_y(_camera);
	var _camera_y2 = camera_get_view_y(_camera)+camera_get_view_height(_camera);
	var _color = $ffffff;
	var _alpha = v_camera_flash;

	draw_set_color(_color);
	draw_set_alpha(_alpha);

	draw_rectangle(_camera_x1,_camera_y1,_camera_x2,_camera_y2,false);
}

//draw outside world mask
var _camera = view_camera[0];
var _camera_x1 = camera_get_view_x(_camera);
var _camera_x2 = camera_get_view_x(_camera)+camera_get_view_width(_camera);
var _camera_y1 = camera_get_view_y(_camera);
var _camera_y2 = camera_get_view_y(_camera)+camera_get_view_height(_camera);
var _color = $000000;
var _alpha = 1;

draw_set_color(_color);
draw_set_alpha(_alpha);

//left boundary
if _camera_x1 <= 0
{
	draw_rectangle(_camera_x1,_camera_y1,0,_camera_y2,false);
}

//right boundary
if _camera_x2 >= room_width
{
	draw_rectangle(_camera_x2,_camera_y1,room_width-1,_camera_y2,false);
}

//top boundary
if _camera_y1 <= 0
{
	draw_rectangle(_camera_x1,_camera_y1,_camera_x2,0,false);
}

//bottom boundary
if _camera_y2 >= room_width
{
	draw_rectangle(_camera_x1,_camera_y2,_camera_x2,room_height-1,false);
}