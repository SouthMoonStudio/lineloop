///create
//variables
v_focus_type = 0;
v_focus_index = noone;
v_focus_point_x = 0;
v_focus_point_y = 0;

v_shake_intensity = 0;
v_shake_intensity_max = 8;
v_shake_cooldown = 0.1;

v_camera_flash = 0;
v_camera_flash_cooldown = 0.1;
