///@function init_display_parameters();
function init_display_parameters()
{
	//variables
		var _input_width = display_get_width();
		var _input_height = display_get_height();
		var _input_aspect_ratio = _input_width/_input_height;
		var _standard_width = 960;
		var _standard_height = 540;
		var _output_width = 0;
		var _output_height = 0;
	
	//keep width
		//_output_width = _standard_width;
		//_output_height = round(_standard_width/_input_aspect_ratio);
	//keep height
		_output_width = round(_standard_height*_input_aspect_ratio);
		_output_height = _standard_height;
	//keep both
		//_output_width = _standard_width;
		//_output_height = _standard_height;
		
	//odd numbers
		if (_output_width & 1)
		{
			_output_width ++;
		}
		if (_output_height & 1)
		{
			_output_height ++;
		}
	
	//create camera
		global.camera = camera_create_view(0,0,_output_width,_output_height,0,noone,0,0,0,0);
	
	//apply camera
		for (var _room=0;_room<=room_last;_room++)
		{
			room_set_view_enabled(_room,true);
			room_set_camera(_room,0,global.camera);
			room_set_viewport(_room,0,true,0,0,_output_width,_output_height);
		}
	
	//resize application surface
		surface_resize(application_surface,_output_width,_output_height);
	
	//resize gui surface
		display_set_gui_size(_output_width,_output_height);
}