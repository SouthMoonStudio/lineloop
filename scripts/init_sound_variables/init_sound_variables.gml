///@function init_sound_variables();
function init_sound_variables()
{
	audio_sound_gain(snd_bg_music,0.5,0);
	audio_sound_gain(snd_lazer,0.8,0);
	audio_sound_gain(snd_hit,0.8,0);
	audio_sound_gain(snd_explode,0.8,0);
	audio_sound_gain(snd_explode2,0.8,0);
	audio_sound_gain(snd_collect,0.8,0);
	audio_sound_gain(snd_special,0.8,0);
	audio_sound_gain(snd_change,0.8,0);

}