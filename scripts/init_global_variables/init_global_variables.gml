///@function init_global_variables();
function init_global_variables()
{
	global.pscore = 0;
	global.font_score = font_add_sprite_ext(spr_font_score,"1234567890",false,4);
	global.quote_count = 15;
	global.gameover_quote[0] = "Oof! That must've hurt";
	global.gameover_quote[1] = "I guess that's one way to get wrecked";
	global.gameover_quote[2] = "That is one way to escape the loop, I think";
	global.gameover_quote[3] = "How many times have you died already?";
	global.gameover_quote[4] = "Just when you think you've got it figured out";
	global.gameover_quote[5] = "That went 'spload quite quickly";
	global.gameover_quote[6] = "Someone once told me that this is not easy";
	global.gameover_quote[7] = "Go grab a cup of coffee, and try again";
	global.gameover_quote[8] = "I blame the game for that one, so should you";
	global.gameover_quote[9] = "Wow! that's a lot of splatter";
	global.gameover_quote[10] = "... 'nuff said";
	global.gameover_quote[11] = "Try: 'there is no spoon'";
	global.gameover_quote[12] = "Did you see that? Did you???";
	global.gameover_quote[13] = "Where's the secret cow level?";
	global.gameover_quote[14] = "Try: 'format c:'";
	global.gameover_quote[15] = "Yup, that's mission accomplished";
}