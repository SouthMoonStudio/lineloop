///@function draw_set_text_parameters();
function draw_set_text_parameters(_font,_halign,_valign,_color,_alpha)
{
	draw_set_font(_font);
	draw_set_halign(_halign);
	draw_set_valign(_valign);
	draw_set_color(_color);
	draw_set_alpha(_alpha);
}