///creation code
instance_create_depth(0,0,-100,obj_controller_camera);
instance_create_depth(0,0,100,obj_controller_background);
instance_create_depth(0,0,0,obj_controller_enemy_spawner);
instance_create_depth(0,0,0,obj_controller_particles);
instance_create_depth(0,0,-1000,obj_controller_game);


var _x = room_width/2;
var _y = room_height/2;
var _depth = -10;
var _object = obj_player;
instance_create_depth(_x,_y,_depth,_object);